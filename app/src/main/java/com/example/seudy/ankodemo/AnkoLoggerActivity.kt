package com.example.seudy.ankodemo


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.info
import org.jetbrains.anko.warn

class AnkoLoggerActivity : AppCompatActivity()/*, AnkoLogger*/{
    // use logger as plain object
    private val logWithSpecificTag = AnkoLogger("msg_tag")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anko_logger)
        /*info("AnkoLoggerActivity info log.")
        debug(5)
        warn(null)*/

        // logger as plain object
        logWithSpecificTag.warn("Logger as plain object, bro!")
    }
}
