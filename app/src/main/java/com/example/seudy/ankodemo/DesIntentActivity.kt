package com.example.seudy.ankodemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import org.jetbrains.anko.toast
import kotlinx.android.synthetic.main.activity_des_intent.*
class DesIntentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_des_intent)
        btn_show.setOnClickListener {
            toast("${intent.getIntExtra("id", 0)}, ${intent.getStringExtra("text")}")
        }
    }
}
