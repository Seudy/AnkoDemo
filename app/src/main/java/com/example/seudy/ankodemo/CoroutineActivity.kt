package com.example.seudy.ankodemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_coroutine.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class CoroutineActivity : AppCompatActivity() {
    companion object{
        val APP_ID = "15646a06818f61f7b8d7823ca833e1ce"
        val URL = "http://api.openweathermap.org/data/2.5/forecast/daily?mode=json&units=metric&cnt=7"
        val ZIP_CODE = "94043"
        val COMPLETE_URL = "$URL&APPID=$APP_ID&q=$ZIP_CODE"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutine)
        tv_url.text = COMPLETE_URL

    }

    override fun onResume() {
        super.onResume()
        loadForeCast()

    }

    /**
     * Async-style function
     */
    /**
     * async() start seperate coroutine which is a light-weight thread that works concurrently with other coroutines
     * async(UI): Dispatch execution onto Android main UI thread and provides native [delay][Delay.delay] support.
     */
    private fun loadForeCast() = async(UI){
        // with bg{...} you can easily execute your code on the background thread
        val result = bg{ java.net.URL(COMPLETE_URL).readText()}
        Log.d("result", result.toString())
        // use await() to get its eventual result
        tv_response.text = result.await()

    }


}
