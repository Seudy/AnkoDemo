package com.example.seudy.ankodemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.appcompat.R.id.add
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_misc.*
import org.jetbrains.anko.*

class MiscActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_misc)

        verticalLayout {
            textView {
                text = "Anko Layout: <*> : tuoyaL oknA"
                gravity = Gravity.CENTER_HORIZONTAL
            }
            button {
                text = "B1"
                backgroundColor = 0xff0000.opaque
            }
            button {
                text = "B2"
                backgroundColor = 0x99.gray.opaque
            }
        }.applyRecursively { view ->
            when (view) {
                is Button -> view.textSize = 20f
            }
        }

    }
}
