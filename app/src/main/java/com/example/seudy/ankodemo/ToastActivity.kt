package com.example.seudy.ankodemo

import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_toast.*
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.custom.customView

class ToastActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toast)

        btn_toast.setOnClickListener { toast("Normal Toast") }

        btn_long_toast.setOnClickListener { longToast("Long Toast") }

        btn_alerts.setOnClickListener {
            alert("Hi, I'm Seudy.", "How are you getting on?"){
                yesButton { toast("Yes, I'm fine.") }
                noButton { toast("No, I'm Screwed.") }
            }.show()
        }

        btn_alert_compat.setOnClickListener {
            alert(Appcompat, "AppCompat Style").show()
        }

        btn_selector.setOnClickListener {
            val countries = listOf("Russia", "USA", "Japan", "Australia")
            selector("Where are you from?", countries){ dialogInterface: DialogInterface, i: Int ->
                toast("So you're living in ${countries[i]}, right?")
            }
        }

        btn_alert_custom.setOnClickListener {
            alert {
                positiveButton("Cool") { toast("Yess!!!") }
                customView {
                    linearLayout {
                        textView("I'm a text")
                        button("I'm a button")
                        padding = dip(16)
                    }
                }
            }.show()
        }

        btn_progress.setOnClickListener {
            //val dialog = progressDialog("Please wait a bit...", "Fetching data").show()
            indeterminateProgressDialog("Please wait a bit...", "Fetching data").show()
        }
    }
}
