package com.example.seudy.ankodemo

import android.renderscript.ScriptGroup
import android.text.InputType
import android.view.Gravity
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick
/**
 * Created by SAMBO on 19-Jun-17.
 */
class LoginUI : AnkoComponent<LayoutActivity>{
    lateinit var  et_id: TextView
    override fun createView(ui: AnkoContext<LayoutActivity>) = with(ui){
        linearLayout {
            orientation = LinearLayout.VERTICAL
            linearLayout {
                lparams(width = matchParent, height = wrapContent){
                    horizontalMargin = dip(16)
                }
                orientation = LinearLayout.HORIZONTAL
                textView {
                    text = "UserId"
                }.lparams {
                    weight = 0.25f
                    width = dip(0f)
                    height = wrapContent
                }
                 et_id = editText {
                    id = R.id.et_id
                    hint = "ID"
                }.lparams {
                    weight = 0.75f
                    width = dip(0f)
                    height = wrapContent
                }

            }
            linearLayout {
                lparams(width = matchParent, height = wrapContent){
                    topMargin = dip(10f)
                    horizontalMargin = dip(16)
                }
                orientation = LinearLayout.HORIZONTAL

                textView {
                    text = "Password"
                }.lparams {
                    weight = 0.25f
                    width = dip(0f)
                    height = wrapContent
                }
                editText {
                    id = R.id.et_pwd
                    hint = "Password"
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                }.lparams {
                    weight = 0.75f
                    width = dip(0f)
                    height = wrapContent
                }
            }

            button("Login"){
                onClick {
                    toast("Hello! ${et_id.text}")
                }
            }.lparams(width = matchParent) {
                topMargin = dip(10)
                horizontalMargin = dip(16)
            }
        }
    }
}