package com.example.seudy.ankodemo

import android.Manifest
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import kotlinx.android.synthetic.main.activity_intent.*
import org.jetbrains.anko.*

class IntentActivity : AppCompatActivity() {
    var  MY_PERMISSIONS_REQUEST_CALL_PHONE: Int = 2017
    var  MY_PERMISSIONS_REQUEST_SEND_SMS: Int = 2018
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CALL_PHONE)) {
                longToast("Please, Allow Permission Call Phone.")
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.CALL_PHONE),
                        MY_PERMISSIONS_REQUEST_CALL_PHONE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
                longToast("Please, Allow Permission Send SMS.")
            }else{
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.SEND_SMS),
                        MY_PERMISSIONS_REQUEST_SEND_SMS)
            }
        }

        btn_intent.setOnClickListener {
            //startActivity(intentFor<IntentActivity>("id" to 5, "text" to "Hello").singleTop())
            startActivity<DesIntentActivity>("id" to 5, "text" to "New")
        }
        btn_intent_call.setOnClickListener { makeCall("01034834748")}

        btn_intent_view.setOnClickListener { browse("http://www.bizplay.co.kr") }

        btn_intent_share.setOnClickListener { share("Hello! World", "Testing") }

        btn_intent_send.setOnClickListener {  }

        btn_intent_email.setOnClickListener { email("seudylim@gmail.com", "Hello!", "Long time no see.")  }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            MY_PERMISSIONS_REQUEST_CALL_PHONE -> {
                if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    longToast("Permission Call Phone is granted.")
                }else{
                    // permission denied
                }
                return
            }
            MY_PERMISSIONS_REQUEST_SEND_SMS -> {
                if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    longToast("Permission Send SMS is granted.")
                }else{
                    // permission denied
                }
                return
            }
        }
    }
}
