package com.example.seudy.ankodemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_intent.setOnClickListener { startActivity<IntentActivity>() }

        btn_toast.setOnClickListener { startActivity<ToastActivity>() }

        btn_logging.setOnClickListener { startActivity<AnkoLoggerActivity>() }

        btn_misc.setOnClickListener { startActivity<MiscActivity>() }

        btn_layout.setOnClickListener { startActivity<LayoutActivity>() }

        btn_coroutine.setOnClickListener { startActivity<CoroutineActivity>() }
    }
}
